use lambda_runtime::{service_fn, LambdaEvent, Error, Context};
use serde::{Deserialize, Serialize};
use std::collections::HashSet;

#[derive(Deserialize, Serialize)]
struct Input {
    data: String,
}

#[derive(Deserialize, Serialize)]
struct Output {
    palindromes: HashSet<String>,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = service_fn(check_palindromes);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn check_palindromes(event: LambdaEvent<Input>) -> Result<Output, Error> {
    let (event, _context): (Input, Context) = event.into_parts();
    let mut palindromes = HashSet::new();
    for word in event.data.split_whitespace() {
        if is_palindrome(word) {
            palindromes.insert(word.to_lowercase());
        }
    }
    Ok(Output { palindromes })
}

fn is_palindrome(word: &str) -> bool {
    let word = word.to_lowercase();
    word.chars().eq(word.chars().rev())
}

