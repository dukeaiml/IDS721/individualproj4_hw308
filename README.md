# Rust AWS Lambda and Step Functions

The palindrome detection project is designed to leverage AWS serverless architecture to efficiently identify and process palindrome words within text data. The project consists of two key AWS Lambda functions implemented in Rust, orchestrated by AWS Step Functions. This setup allows for the modular handling of data processing tasks, ensuring scalability and flexibility. The Lambda functions are designed to first identify palindrome words and then filter these to find palindromes exceeding a specified character length. 


## Getting started

**1. Rust AWS Lambda Functions**

a. check_palindromes

This Lambda function processes input text to check each word and determine if it is a palindrome. The function extracts words from the input, converts them to lowercase for uniformity, and checks each word against its reverse.

Input: A string of text.

Processing: Splits the text into words, normalizes them to lowercase, and checks each word to determine if it is a palindrome.

Output: A list (or set) of words identified as palindromes.

b. filter_long_palindromes

After check_palindromes, this Lambda function filters the list of detected palindrome words to include only those whose lengths exceed five characters. This helps in focusing on more substantial palindrome entries for further processing or analysis.

Input: The list of palindrome words from the previous Lambda function.

Processing: Iterates through the list of palindromes and filters those that are more than five characters.

Output: A list (or set) of palindromes longer than five characters.


**2. Step Functions workflow coordinating Lambdas**

Start State: Triggers the check_palindromes function with input text.

Intermediate State: Passes the output from check_palindromes to filter_long_palindromes.

End State: Collects the output from filter_long_palindromes and ends the workflow, optionally triggering further actions or notifications based on the results.

**3. Orchestrate data processing pipeline**

The data processing pipeline is orchestrated through the Step Functions, ensuring that each step is executed in sequence and managing the data flow between Lambda functions.

The text data is initially input into the check_palindromes function.

The identified palindromes are then passed to filter_long_palindromes.

The final output is the list of palindromes longer than five characters, which can be stored, further processed, or used to trigger additional workflows.

**4. Demo Video**

Demo video is attached above as `demo.mov`.

## Procedures with Screenshots

**1. Create these two lambda projects, then build and deploy on AWS lambda.** 
```
# Create project
cargo create new project

# Add code in main.rs and Cargo.toml

# local test
cargo lambda watch
cargo lambda invoke --data-ascii <input>

# Build and deploy on AWS
cargo lambda build --release
cargo lambda deploy --region <region>. --iam-role <role>
```

![](images/func1.png)
![](images/func2.png)


**2. Create State machine of Step function (see the attached ASL.json for more)**

![](images/step_func.png)

**3. Execute the state machine and see its corresponding graph view and output on AWS**
Below shows the step1_input, step1_output, step2_input, step2_output.

![](images/step1_in.png)
![](images/step1_out.png)
![](images/step2_in.png)
![](images/step2_out.png)

**4. Add yml file and build pipeline on gitlab. (see .gitlab-ci.yml file)**




