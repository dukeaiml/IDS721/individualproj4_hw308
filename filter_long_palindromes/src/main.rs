use lambda_runtime::{service_fn, LambdaEvent, Error, Context};
use serde::{Deserialize, Serialize};
use std::collections::{HashSet, BTreeSet};

#[derive(Deserialize, Serialize)]
struct Input {
    palindromes: HashSet<String>,
}

#[derive(Deserialize, Serialize)]
struct Output {
    long_palindromes: BTreeSet<String>,  // Using BTreeSet to maintain sorted order
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = service_fn(filter_long_palindromes);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn filter_long_palindromes(event: LambdaEvent<Input>) -> Result<Output, Error> {
    let (event, _context): (Input, Context) = event.into_parts();
    let long_palindromes: BTreeSet<String> = event.palindromes.into_iter()
        .filter(|word| word.len() > 5)
        .collect();
    Ok(Output { long_palindromes })
}

